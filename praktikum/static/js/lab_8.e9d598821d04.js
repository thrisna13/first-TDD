 var loginFlag = false;
  // FB initiation function
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '134580867247489',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.11'
    });
    FB.getLoginStatus(function (response) {
      if (response.status === "connected") {
        console.log("yeay! we are logged in!");
        render(true);
        
      }
      else if (response.status === "not_authorized") {
        console.log("not logged into apps yet");
        render(false);
        
      }
      else {

        console.log("not logged into fb yet");
        render(false);
        
      }
    })
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

const render = loginFlag => {
    if (loginFlag) {
      // Jika yang akan dirender adalah tampilan sudah login

      console.log("tampilan saat sudah login");
      getUserData(user => {
        // Render tampilan profil, form input post, tombol post status, dan tombol logout
        $('#lab8').html(
          
          '<div class="container" style="padding-top: 0px">'+
            '<div class="row">'+
              '<div class="col-sm-8 col-sm-offset-2">'+
                '<div class="card">'+
                  '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
                  '<img class="picture img-circle" src="' + user.picture.data.url + '" alt="profpic" />' +
                  '<h1>'+user.name+'</h1>'+
                  '<h3>'+user.about+'</h3>'+
                  '<h3>'+ user.email + ' - ' + user.gender + '</h3>'+
                  '<button class="logout btn btn-warning" onclick="facebookLogout()">Logout</button>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="container" style="padding-top: 30px; padding-bottom:40px">'+
            '<div class="row">'+
              '<div class="col-sm-8 col-sm-offset-2">'+
              '<div class="form-group">'+
                '<input id="postInput" type="text" class="form-control input-lg post" placeholder="Ketik Status Anda" />' +
              '</div>'+
                '<button class="postStatus btn btn-info btn-lg btn-block" style="float:left; " onclick="postStatus()">Post ke Facebook</button>' +
                
                '</div>'+
            '</div>'+
          '</div>'
        );
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            if (value.message && value.story) {
              $('#lab8').append(
                '<section name="my-list" id="show-status" style="padding-bottom: 20px; padding-top: 0px">'+
                  '<div class="container" style="padding-top: 0px">'+
                    '<div class="row">'+
                      '<div class="col-sm-8 col-sm-offset-2">'+
                    '<div class="media" style="padding: 40px">'+
                      '<div class="media-left">'+
                        '<img src="'+user.picture.data.url+'" class="media-object img-circle" style="width:75px">'+
                      '</div>'+
                      '<div class="media-body">'+
                        '<div class="feed">' +
                          '<h3>' + value.message + '</h3>' +
                          '<h4>' + value.story + '</h4>' +
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                  
                '</section>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<section name="my-list" id="show-status" style="padding-bottom: 20px; padding-top: 0px">'+
                  '<div class="container" style="padding-top: 0px">'+
                    '<div class="row">'+
                      '<div class="col-sm-8 col-sm-offset-2">'+
                    '<div class="media" style="padding: 40px">'+
                      '<div class="media-left">'+
                        '<img src="'+user.picture.data.url+'" class="media-object img-circle" style="width:75px">'+
                      '</div>'+
                      '<div class="media-body">'+
                        '<div class="feed">' +
                          '<h4>' + value.message + '</h4>' +
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                    '</div>'+
                    '</div>'+
                '</section>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<section name="my-list" id="show-status" style="padding-bottom: 20px; padding-top: 0px">'+
                  '<div class="container" style="padding-top: 0px">'+
                    '<div class="row">'+
                      '<div class="col-sm-8 col-sm-offset-2">'+
                    '<div class="media" style="padding: 40px">'+
                      '<div class="media-left">'+
                        '<img src="'+user.picture.data.url+'" class="media-object img-circle" style="width:75px">'+
                      '</div>'+
                      '<div class="media-body">'+
                        '<div class="feed">' +
                          '<h4>' + value.story + '</h4>' +
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                    '</div>'+
                    '</div>'+
                '</section>'
              );
            }
          });
        });

      });

    } else {
      // Tampilan ketika belum login
      $('#lab8').html('<div class="col-sm-4"><button class="btn btn-block btn-social btn-facebook" onclick="facebookLogin()"><span class="fa fa-facebook"></span>Sign in with Facebook</button></div>');
    }
  };
  
  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
    FB.login(function(response){
       console.log(response);
       if (response.status === "connected") {
        console.log("yeay! we are logged in!");
        loginFlag = true;
        render(loginFlag);
      }
      else if (response.status === "not_authorized") {
        console.log("not logged into apps yet");
      }
      else {
        console.log("not logged into fb yet");
      }
     }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})

  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
    console.log("is this jalan?");
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.logout();
          render(false);
        }
     });

  };

  // TODO: Lengkapi Method Ini
  // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
  // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
  // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
  // meneruskan response yang didapat ke fungsi callback tersebut
  // Apakah yang dimaksud dengan fungsi callback?
  const getUserData = (fun) => {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name,cover,picture,gender,about,email', 'GET', function(response){
            console.log(response);
            fun(response);
          });
        }
    });

  };


  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          /* make the API call */
          FB.api(
              "/me/feed",
              function (response) {
                if (response && !response.error) {
                  /* handle the result */
                  console.log(response);
                  fun(response);
                }
              }
          );
        }
    });
  };
  const postFeed = (statusnya) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    
     FB.api('/me/feed', 'POST', {message:statusnya});

  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    render(true);
  };
