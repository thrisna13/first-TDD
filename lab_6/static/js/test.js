$( document ).ready(function() {
    const button_8 = $('button:contains("8")');
    const button_4 = $('button:contains("4")');

    const button_add = $('button:contains("+")');
    const button_sub = $('button:contains("-")');
    const button_mul = $('button:contains("*")');
    const button_div = $('button:contains("/")');

    const button_clear = $('button:contains("AC")');
    const button_res = $('button:contains("=")');

    const textarea_chatbox = $('#textarea-chat');

    QUnit.test( "Addition Test", function( assert ) {
      button_8.click();
      button_add.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 12, "8 + 4 must be 12" );
      button_clear.click();
    });

    QUnit.test( "Substraction Test", function( assert ) {
      button_8.click();
      button_sub.click();
      button_4.click();
      button_res.click();
      assert.notEqual( $('#print').val(), 12, "8 - 4 must be 4" );
      button_clear.click();
    });

    QUnit.test( "Multiply Test", function( assert ) {
      button_8.click();
      button_mul.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 32, "8 * 4 must be 32" );
      button_clear.click();
    });

    QUnit.test( "Division Test", function( assert ) {
      button_8.click();
      button_div.click();
      button_4.click();
      button_res.click();
      assert.equal( $('#print').val(), 2, "8 / 4 must be 2" );
      button_clear.click();
    });

    // TEST chatbox
    QUnit.test("Chatbox Enter", function(assert){
      const text = 'hehehe ini test';
      textarea_chatbox.val(text);
      
      // press enter
      var event = jQuery.Event("keypress");
      event.which = 13;
      event.keyCode = 13;
      textarea_chatbox.trigger(event);
      
      var check = $('#msg-send-0').text();
      assert.equal(check, text);
    });
});
