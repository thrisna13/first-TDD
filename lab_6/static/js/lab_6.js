// Chatbox

var sendRecieve = true;
var chat_id = 0;
const sendChat = function(event){
  // https://www.w3schools.com/jquery/jquery_dom_add.asp
  // https://stackoverflow.com/questions/5677799/how-to-append-data-to-div-using-javascript
  // https://stackoverflow.com/questions/15310647/why-keydown-event-works-like-keypress-event

  const textarea = document.getElementById("textarea-chat");

  if(event.keyCode == 13 && event.type == "keypress"){
    var text = textarea.value;
    textarea.value = "";

    if(text.length > 0){
      var content = document.createTextNode(text);
      
      var newBubble = document.createElement("div");
      newBubble.className = "msg-send";
      newBubble.id = "msg-send-"+chat_id++;
      newBubble.appendChild(content);  
      document.getElementById("msg-insert").appendChild(newBubble);
  
      var httpRequest = new XMLHttpRequest();
      httpRequest.onreadystatechange = function(){
        // Process the server response here.
        if(httpRequest.readyState === 4 && httpRequest.status == 200){
          var dataGet = JSON.parse(httpRequest.responseText);
          console.log(dataGet);
          if(dataGet.result == 100){
            var newBubble = document.createElement("div");
            newBubble.className = "msg-receive";
            var res = document.createTextNode(dataGet.response);
            newBubble.appendChild(res);
            
            document.getElementById("msg-insert").appendChild(newBubble);
          }
        }
      };  
      // http://developer.simsimi.com/api
      // need to use cors proxy karena yang kita kirim harus dari alamat yang sama
      const corsProxy = 'https://cors-anywhere.herokuapp.com/';
      var req = 'http://sandbox.api.simsimi.com/request.p?key=3571d39e-cf08-4aa1-b9d7-54eced4435cd&lc=id&ft=0.0&text='+text;
      httpRequest.open("GET", corsProxy+req, true);
      httpRequest.send();
  
    }
    return false;

  } 

  return true;
}


var chatboxHidden = false;
const turnDownChatbox = function() {
  // https://www.w3schools.com/jquery/jquery_css.asp

  const chatbody = $(".chat-body");

  if(chatboxHidden){
    chatbody.css({"height":"355px","margin-bottom":"45px"});
    $(".chat-text").css("height", "45px");
    chatboxHidden = false;
  } else {
    chatbody.css({"height":"0","margin-bottom":"0"});
    $(".chat-text").css("height", "0");
    chatboxHidden = true;
  }
  
  $(".chat-button").css("transform", "rotate(180deg)");
}


// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = null;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

// GANTI TEMA

const themes = '[\
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},\
  {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},\
  {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},\
  {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},\
  {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},\
  {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},\
  {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},\
  {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},\
  {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},\
  {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},\
  {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}\
]';

const dataThemes = JSON.parse(themes);

applytheme = function(theme){
  $('body').css('background', theme.bcgColor);
  $('html').css('color', theme.fontColor);
}

getThemeById = function(id) {
  id = Number(id);

  for(var i=0; i<dataThemes.length; ++i){
    var theme = dataThemes[i];

    if(theme.id == id)
      return theme;
  }

  return null;
}

$(document).ready(function() {

  if(typeof(Storage) !== "undefined") {
  
    // RESET
    if(localStorage.themes == null || localStorage.selectedTheme == null){
      localStorage.themes = themes;
      localStorage.selectedTheme = '{"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"}';
    }
    
    console.log(localStorage.selectedTheme);
    applytheme(JSON.parse(localStorage.selectedTheme));
  
  } else {
    alert("browser doesn't support");
  }
    
  $('.my-select').select2({
    'data': dataThemes,
  });

  $('.apply-button').on('click', function(){
    // [TODO] ambil value dari elemen select .my-select
    var select = $('.my-select').select2('data')[0];
    var selected = getThemeById(select.id);
    applytheme(selected);
    console.log('hehe');
    localStorage.selectedTheme = JSON.stringify(selected);
  })

});
