from django.shortcuts import render

response = {
    'author':'Thrisnadevany',
}
# Create your views here.
def index(request):
    template = 'lab_6/lab_6.html'
    return render(request, template, response)
